variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {}
variable "AMI" {
  type = map(string)
  default = {
    eu-central-1 = "ami-0767046d1677be5a0"
  }
}
