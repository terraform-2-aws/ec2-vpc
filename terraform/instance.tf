
resource "aws_instance" "mainInstance" {
  ami           = lookup(var.AMI, var.AWS_REGION)
  instance_type = "t2.micro"

  tags = {
    Name = "mainInstance"
  }
}

# output "ip" {
#   value = "${aws_instance.example.public_ip} >> public_ip.txt"
# }

