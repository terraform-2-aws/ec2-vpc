AWS :cloud: Terraform  and CI/CD :fox:

**Intro**:

- This repository creates VPC with six different subnets as illustrated in below:


![](Structure.jpeg)


- The pipeline is used to apply different stages for terraform. From init to plan and apply.
- plan and apply steps are set as manual to avoid unintentional resource creation.
- In `/terraform` folder you can find the necessary code which can create the whole infrastructure
- the output of the terraform plan is saved in a json file which later will be used for resource creation.

![](pipeline.PNG)

